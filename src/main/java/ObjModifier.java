import java.nio.FloatBuffer;

import static java.util.Objects.isNull;

public class ObjModifier {
    public static void centerVertices(FloatBuffer buf) {
        // We often get 3D models which are not positioned at the origin of the coordinate system. Placing such models
        // on a 3D map is harder for users. One solution is to reposition such a model so that its center is at the
        // origin of the coordinate system. Your task is to implement the centering.
        //
        // FloatBuffer stores the vertices of a mesh in a continuous array of floats (see below)
        // [x0, y0, z0, x1, y1, z1, ..., xn, yn, zn]
        // This kind of layout is common in low-level 3D graphics APIs.
        // TODO: Implement your solution here


        // get all coordinates to array
        float[] floatArray = new float[buf.limit()];
        buf.get(floatArray);

        Float xMin = null;
        Float yMin = null;
        Float zMin = null;
        Float xMax = null;
        Float yMax = null;
        Float zMax = null;

        for(int i = 0; i < floatArray.length; i++){
            if (i%3 == 0) {
                if (isNull(xMax) || floatArray[i] > xMax) xMax = floatArray[i];
                if (isNull(xMin) || floatArray[i] < xMin) xMin = floatArray[i];
            } else if (i%3 == 1) {
                if (isNull(yMax) || floatArray[i] > yMax) yMax = floatArray[i];
                if (isNull(yMin) || floatArray[i] < yMin) yMin = floatArray[i];
            } else {
                if (isNull(zMax) || floatArray[i] > zMax) zMax = floatArray[i];
                if (isNull(zMin) || floatArray[i] < zMin) zMin = floatArray[i];
            }
        }

        // calculate change values
        float xChange = xMin + (xMax - xMin) / 2;
        float yChange = yMin + (yMax - yMin) / 2;
        float zChange = zMin + (zMax - zMin) / 2;


        // update FloatBuffer
        for(int i = 0; i< floatArray.length; i++){
            var previous = buf.get(i);
            if (i%3 == 0) {
                buf.put(i, previous - xChange);
            } else if (i%3 == 1) {
                buf.put(i, previous - yChange);
            } else {
                buf.put(i, previous - zChange);
            }
        }
    }
}
